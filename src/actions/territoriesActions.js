import {FETCH_TERRITORIES,SET_SELECTED_TERITORY} from './types';


export const fetchTerritories = () => dispatch => {
        fetch('http://localhost:3000/territories')
        .then(res => res.json())
        .then(territories =>
            dispatch({
            type:FETCH_TERRITORIES,
            payload: territories
        })
    );
};



export const setSelectedTeritory= (teritoryId) => {
    return ({
            type:SET_SELECTED_TERITORY,
            payload:teritoryId
    })
};
