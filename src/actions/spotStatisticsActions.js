import {FETCH_SPOT_STATISTICS} from './types';


export const fetchSpotStatistics = () => (dispatch,getState) => {
    const state = getState();
    const scanId = state.scans.selectedScan;
    const platformId = state.platforms.selectedPlatform;
    const teritoryId = state.territories.selectedTeritory;

    let url = `http://localhost:3000/spot_statistics`

         url = ((scanId!=null) && url + `?scanId=${scanId}`)
         url = (platformId!=null) && ( (scanId!=null) ? url+`&platformId=${platformId}` : url+`?platformId=${platformId}`)
         url = teritoryId && (platformId ? url+`&teritoryId=${teritoryId}` : `?teritoryId=${teritoryId}`)
    
    fetch(url)
    .then(res =>  res.json())
    .then(statistics => dispatch({
        type:FETCH_SPOT_STATISTICS,
        payload:statistics
    })
);
};