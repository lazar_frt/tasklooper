import {FETCH_TERRITORIES, SET_SELECTED_TERITORY} from '../actions/types'


const initialState = {
    items: [],
    selectedTeritory:null
}

export default function(state = initialState, action) {
    switch(action.type){
        case FETCH_TERRITORIES:
            return {
                ...state,
                items:action.payload
            }
        case SET_SELECTED_TERITORY:
            return {
                ...state,
                selectedTeritory:action.payload
            }
        default:
            return state;
    }
}