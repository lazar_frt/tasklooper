import React from 'react';
import MainNav from './components/MainNav';
import AppInsights from './components/AppInsights';
import DropdownSection from './components/DropdownSection';
import {Provider} from 'react-redux';
import store from './store'


function App() {
  return (
    <Provider store={store}>
        <MainNav />
        <AppInsights />
        <DropdownSection />
    </Provider>
  );
}
export default App;

