import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import PopoverScreenshot from './PopoverScreenshot';
import PositionAlert from './PositionAlert';


class TableSection extends Component {

  render() {
    // const eyeIcon = (<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    //   <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z" />
    //   <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
    // </svg>);
    
    // const previewScreenshot = (<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-view-stacked" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    //   <path fill-rule="evenodd" d="M3 0h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3zm0 8h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1H3z" />
    // </svg>);

  
    const spotStatisticItems = this.props.items.map((item,i) => (
      <tr key={item.id}>
        <td>{item.section_name}</td>
        <td>{item.platform_name}</td>
        <td>{item.spot_type}</td>
        {/* <PositionAlert row position={item.position} index={i} positionOld={item.old_position}/>
        <PositionAlert column position={item.position_col} positionOld={item.old_position_col}/> */}
        <td className="aroundsp">
          <div>
            {item.title_type}
          </div>
              {/* {eyeIcon} */}
        </td>
        <td>Path{`>`}Second Step{`>`}Third Step</td>
        <td className="go-right">
          {/* <PopoverScreenshot>
            {previewScreenshot}
          </PopoverScreenshot> */}
        </td>
      </tr>
    ));

    return (
      <Table className="font-resize">
        <thead className="bg-thd">
          <tr>
            <th>Section</th>
            <th>Page Name</th>
            <th>Spot Type</th>
            <th className="go-right">Row(old position)</th>
            <th className="go-right">Column(old position)</th>
            <th>App/Title</th>
            <th>User Path</th>
            <th className="go-right">Screenshot</th>
          </tr>
        </thead>
        <tbody>
          {spotStatisticItems}
        </tbody>
      </Table>
    )
  }
}

export default TableSection;
