import React from 'react'
import { Navbar, Nav ,Image,Row} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import ImgLooper from '../images/looper-inline.png';
import ImgDisnay from '../images/disney.png';
import './Custom.css';



function MainNav() {
    return (
        <Navbar collapseOnSelect expand="lg" bg="white" variant="light" className="mainHe">
            <Navbar.Brand href="#home">
                <Image src={ImgLooper} className="img-resize" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto change-font">
                    <Nav.Link className="marg-right">Dashboard</Nav.Link>
                    <Nav.Link className="marg-right">Pricing & Avaiability</Nav.Link>
                    <Nav.Link className="marg-right">Artwork</Nav.Link>
                    <Nav.Link className="marg-right">Merchandising</Nav.Link>
                    <Nav.Link className="marg-right">Merchandising</Nav.Link>
                    <Nav.Link className="marg-right">Analystics</Nav.Link>
                </Nav>
                <Nav>
                    <Nav.Link href="#deets">
                        <Image src={ImgDisnay} className="img-resize-right" />
                    </Nav.Link>
                    <Nav.Link href="#memes" className="account-position">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-person" fill="dark" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                        </svg>
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
export default MainNav;
