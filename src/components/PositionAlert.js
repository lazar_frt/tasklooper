import React from 'react'
import styled, { css } from 'styled-components'
import { Row, Col,Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import arrowUp from '../images/arrowUp.png'
import arrowDown from '../images/arrowDownRed.png'
import arrowLeft from '../images/arrowLeft.png'
import arrowRight from '../images/arrowRightRed.png'



const Alert = styled.td`
    ${props => (props.position<props.positionOld || props.position!=0) && css `
        background: #e3f3e8;
        color:green;
    `}
    ${props => (props.position>props.positionOld) && css `
        background: #f7dce8;
        color:red;
    `}
`;


function PositionAlert(props) {

    return (
        <>
            {/* ***ROW*** */}
            {props.row &&
               ( <Alert row position={props.position} positionOld={props.positionOld} key={props.index}> 
               {props.position<props.positionOld ? 
               (<Row>
                   {props.position===0 ? <Col>{' '}</Col> :
                   <Col>
                    <Image src={arrowUp} className="imgResize"/>
                   </Col>}
                   {props.position>0 ? <Col>
                    {props.position}
                   </Col> : <Col>{' '}</Col>}
                   <Col>
                    {'('}{props.positionOld}{')'}
                   </Col>
               </Row>) : 
               (<Row>
                {props.position===0 ?<Col>{' '}</Col> :
                <Col>
                 <Image src={arrowDown} className="imgResize"/>
                </Col>}
                {props.position>0 ? <Col>
                    {props.position}
                   </Col> : <Col>{' '}</Col>}
               {(props.positionOld===0 && props.position===0) ? (<Col>{'--'}</Col>) :
                <Col>{'('}{props.positionOld}{')'}</Col>}
                </Row>)}
                </Alert>
               )
            }
            {/* ***COLUMN*** */}
            {props.column &&
               ( <Alert row position={props.position} positionOld={props.positionOld} key={props.index}> 
               {props.position<props.positionOld ? 
               (<Row>
                   {props.position===0 ? <Col>{' '}</Col> :
                   <Col>
                    <Image src={arrowLeft} className="imgResize"/>
                   </Col>}
                   {props.position>0 ? <Col>
                    {props.position}
                   </Col> : <Col>{' '}</Col>}
                   <Col>
                    {'('}{props.positionOld}{')'}
                   </Col>
               </Row>) : 
               (<Row>
                {props.position===0 ?<Col>{' '}</Col> :
                <Col>
                 <Image src={arrowRight} className="imgResize"/>
                </Col>}
                {props.position>0 ? <Col>
                    {props.position}
                   </Col> : <Col>{' '}</Col>}
               {(props.positionOld===0 && props.position===0) ? (<Col>{'--'}</Col>) :
                <Col>{'('}{props.positionOld}{')'}</Col>}
                </Row>)}
                </Alert>
               )
            }
        </>
    );
}

export default PositionAlert;
