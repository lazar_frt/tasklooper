import React,{Component} from 'react'
import { Row, Col,Image } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import styled from 'styled-components';
import ImageTV from '../images/appleTV.png';
import './Custom.css';
import Dropdown from './Dropdown';
import {connect} from 'react-redux';
import {fetchScans} from '../actions/scanActions';
import {fetchPlatforms} from '../actions/platformActions';
import {fetchTerritories} from '../actions/territoriesActions';
import TableSection from './TableSection';
import {fetchSpotStatistics} from '../actions/spotStatisticsActions';


const Container = styled.div`
    margin:1em;
`;


class DropdownSection extends Component {
    componentDidMount(){
        this.props.fetchScans();
        this.props.fetchPlatforms();
        this.props.fetchTerritories();
        this.props.fetchSpotStatistics();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.scanId !== this.props.scanId || prevProps.platformId !== this.props.platformId || prevProps.teritoryId !== this.props.teritoryId)
        {
            this.props.fetchSpotStatistics();
        }
    }

    render() {
        return (
            <>
            <Container>
            <Row>
                <Col md={2}>
                    <Image src={ImageTV} className="tv-margin" />
                </Col>
                <Col md={2} className="mt-2">
                    <Dropdown type='scan' items={this.props.scans} subtitle='COMPARE'/>
                </Col> 
                <Col md={2} className="mt-2">
                    <Dropdown type='platform' items={this.props.platforms} subtitle='PLATFORM'/>
                </Col>
                   <Col md={2} className="mt-2">
                    <Dropdown type='teritories' items={this.props.territories} subtitle='Region'/>
                </Col>
            </Row>
        </Container>
        <TableSection items={this.props.spotStatistics} />
        </>
        )
    }
}

const mapStateToProps = state => {
    return {
        scans:state.scans.items,
        platforms: state.platforms.items,
        territories: state.territories.items,
        spotStatistics:state.statistics.items,
        scanId:state.scans.selectedScan,
        platformId: state.platforms.selectedPlatform,
        teritoryId: state.territories.selectedTeritory
    }
}

const mapDispatchToProps = dispatch => {
    return {
      fetchScans: () => dispatch(fetchScans()),
      fetchPlatforms: () => dispatch(fetchPlatforms()),
      fetchTerritories: () => dispatch(fetchTerritories()),
      fetchSpotStatistics: () => dispatch(fetchSpotStatistics())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DropdownSection);
