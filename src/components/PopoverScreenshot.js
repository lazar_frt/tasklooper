import React from 'react'
import { Popover, OverlayTrigger,Image} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import screenshotImage from '../images/screensOne.png'

function PopoverScreenshot() {

    const previewScreenshot = (<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-view-stacked" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M3 0h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3zm0 8h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1H3z" />
    </svg>);
    
   
 const downIcon = (<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-file-arrow-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
 <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H4z"/>
 <path fill-rule="evenodd" d="M8 5a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 9.293V5.5A.5.5 0 0 1 8 5z"/>
</svg>);

    return (
        <OverlayTrigger
          trigger="hover"
          key='left'
          placement='left'
          delay={{ show: 150, hide: 1000 }}
        
          overlay={
            <Popover id={`popover-positioned-left`}>
              <Popover.Content>
                <Image className="popImg-size" src={screenshotImage} />
                <a href={screenshotImage} download>{downIcon}CLICK TO DOWNLOAD</a>
              </Popover.Content>
            </Popover>
          }
        >
          {previewScreenshot}
        </OverlayTrigger>
    )
}
export default PopoverScreenshot
