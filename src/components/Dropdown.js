import React, { Component } from 'react'
import moment from 'moment';
import {connect} from 'react-redux';
import { setSelectedScan } from '../actions/scanActions';
import {setSelectedPlatform} from '../actions/platformActions';
import {setSelectedTeritory} from '../actions/territoriesActions';

class Dropdown extends Component {

     handleDropdownChange = (e) => {

         if(this.props.type==='scan'){
            this.props.setSelectedScan(e.target.value)
         }
         else if (this.props.type==='platform'){
            this.props.setSelectedPlatform(e.target.value)
         }
          else {
             this.props.setSelectedTeritory(e.target.value)
          }
    }


    render() {
        const dropDownOptions = this.props.items.map(item => (
        item.name ? (<option value={item.id} key={item.id}>{item.name}</option>) : (<option value={item.id} key={item.id}>{moment(item.created_at).format('DD MMMM YYYY')}</option>) 
        ));
        return (
        <div class="select">
            <select onChange={this.handleDropdownChange}>
               {dropDownOptions}
            </select>
            <div className="select_arrow">
            </div>
            <div className="select_title">
               {this.props.subtitle}
            </div>
        </div>
        )
    }
}
 


const mapDispatchToProps = dispatch => {
    return {
      setSelectedScan: (scanId) => dispatch(setSelectedScan(scanId)),
      setSelectedPlatform: (platformId) => dispatch(setSelectedPlatform(platformId)),
      setSelectedTeritory: (teritoryId) => dispatch(setSelectedTeritory(teritoryId))
    }
}

export default connect(null,mapDispatchToProps)(Dropdown)
